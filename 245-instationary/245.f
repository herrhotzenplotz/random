      program instationary_field

      implicit none

      integer, parameter :: iterations = 500, x_dim = 100, y_dim = 100
      integer            :: p, x, y
      real               :: delta_x, T_field(x_dim, y_dim), delta_time
      real               :: fourier_number, alpha
      character (len=30) :: output_file = "output.dat"

c     Starting conditions
      do x=1,x_dim
         do y=1,y_dim
            if ((x - 50) ** 2 + (y - 50) ** 2 .LT. 33 ** 2) then
               T_field(x,y) = 273.15 + 300.0
            else
               T_field(x,y) = 273.15
            end if
         end do
      end do

c     This should ideally be read from a config file
      delta_x    = 0.05
      delta_time = 0.00001
      alpha      = 55.9

      fourier_number = alpha * delta_time / (delta_x * delta_x)

c     Stability criteria
      if (fourier_number .GT. 0.25) then
         write (*, *) "WARNING : System is unstable."
      end if

      print *, "== Finite-Differences =="
      print *, "========================"
      print *, "Output file is at ", output_file
      print *, "x_dim", x_dim
      print *, "y_dim", y_dim
      print *, "Fourier-Parameter", fourier_number
      print *, "iterations", iterations

      open(unit=10, file=output_file, status="replace")

      do p = 1, iterations
         T_Field = next_field(T_field, fourier_number)
         write(10, '(100F20.3)') T_field
         write(10, *) ""
      end do

      contains

      function next_field(T_field, fourier_parameter)
         implicit none

         real, intent(in) :: T_field(x_dim, y_dim), fourier_parameter
         real             :: next_field(x_dim, y_dim), a
         integer          :: x, y

         do x = 2, x_dim - 1
            do y = 2, y_dim - 1
               a =     T_field(x+1,y)+T_field(x-1,y)
               a = a + T_field(x,y+1)+T_field(x,y-1)
               next_field(x, y) = fourier_parameter * a
               a = (1.0 - 4.0 * fourier_parameter) * T_Field(x, y)
               next_field(x, y) = next_field(x, y) + a
            end do
         end do
      end function next_field

      end program
