#!/bin/sh

trap "exit 1" INT
args=`getopt bw:h: $*`

if [ $? -ne 0 ]; then
    echo "usage: ./plot.sh [-b] [-w width] [-h height]"
    exit 2
fi

set -- $args

WIDTH=1920
HEIGHT=1080

while :; do
    case "$1" in
        -b)
            shift
            rm -f 245
            rm -f output.dat
            gfortran10 -O1 -o 245 245.f
            ./245
            ;;
        -h)
            shift
            HEIGHT="$1"
            shift
            ;;
        -w)
            shift
            WIDTH="$1"
            shift
            ;;
        --)
            shift
            break
            ;;
    esac
done

rm -rf plots
rm -f plot.mp4
mkdir plots

ITERATIONS=500

for id in $(seq $ITERATIONS); do
    printf "\rPlotting %3d / $ITERATIONS..." $id
    sed "{ 1,$(($id * (100 + 1)))d
$((($id + 1) * (100 + 1)))q
}" output.dat | \
        gnuplot -e "\
set terminal png size ${WIDTH},${HEIGHT}; \
set title 'Forward-Method'; \
set ticslevel 0; \
set view 50,50; \
set xrange [1:100]; \
set yrange [1:100]; \
set zrange [0:600]; \
set cbtics scale 0; \
set output 'plots/`printf %03d $id`.png'; \
set pm3d scansbackward interpolate 0,0; \
splot '-' matrix with pm3d title 'Temperature'" 2>/dev/null
done

printf "\nMerging into video..."

ffmpeg -v 8 -r 60 -f image2 -s "${WIDTH}x${HEIGHT}" -i plots/%03d.png -vcodec libx264 -crf 25  -pix_fmt yuv420p plot.mp4
rm -rf plots
printf "\nDone\n"
