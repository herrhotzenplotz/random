module clmesh;

import std.algorithm;
import std.array;
import std.format;
import std.range;

import sn.cl;

struct vertex
{
    double X, Y, Z, T;
    int    Id;
    int[]  Bonds;
}

struct resultvertex
{
    double X, Y, Z, T;
    int    Id;
}

void
AddBond(ref vertex Vtx, int Id)
{
    assert(!Vtx.Bonds.canFind(Id), "Vertex ");
    Vtx.Bonds ~= Id;
}

struct materialprops
{
    double Density, HeatCapacity, HeatConductivity;
}

struct mesh
{
    vertex[]      Verticies;
    materialprops Props;
}

void
AddBond(ref mesh Mesh, int From, int To)
{
    assert(From < Mesh.Verticies.length, "Bond source is not in mesh");
    Mesh.Verticies[From].AddBond(To);
}

struct meshcomputejob
{
    private struct device_v3
    {
        double X, Y, Z;
    }

    private struct device_vtx
    {
        device_v3           X;
        double              T;
        int                 Id;
        int[MaxDeviceBonds] Bonds;
        size_t              BondsSize;
        static enum MaxDeviceBonds = 64; /* keep this in sync with fourier.cl */
    }

    private {
        ClContext              *Context;
        ClCommandQueue         *Queue;
        ClProgram               Program;
        ClKernel                FourierKernel;
        ClBuffer!device_vtx[2]  Buffers;
        size_t                  CurrentInputBuffer = 0;
        materialprops           Properties;
    }

    private static device_vtx CvtToDeviceVtx(inout vertex Vtx)
    {
        device_vtx OutVtx;

        OutVtx.X.X       = Vtx.X;
        OutVtx.X.Y       = Vtx.Y;
        OutVtx.X.Z       = Vtx.Z;
        OutVtx.T         = Vtx.T;
        OutVtx.Id        = Vtx.Id;
        OutVtx.BondsSize = Vtx.Bonds.length;

        assert(Vtx.Bonds.length <= device_vtx.MaxDeviceBonds,
               "Too many vertex bonds for vertex#%d".format(Vtx.Id));

        import core.stdc.string : memcpy;
        memcpy(OutVtx.Bonds.ptr, Vtx.Bonds.ptr, int.sizeof * OutVtx.BondsSize);

        return OutVtx;
    };

    static meshcomputejob
    CreateComputeJob(ref ClContext Ctx, ref ClCommandQueue Queue, ref const mesh Mesh)
    {
        meshcomputejob Job;

        Job.Program       = Ctx.CreateProgram(import("fourier.cl"));
        Job.FourierKernel = Job.Program.CreateKernel("fourier");

        static foreach (i; iota(0, 2)) {
            Job.Buffers[i] = Ctx.CreateBuffer!device_vtx(Mesh.Verticies.length, CL_MEM_READ_WRITE);
            Job.Buffers[i].HostBuffer = Mesh.Verticies.map!(CvtToDeviceVtx).array;
            Queue.CopyToDevice(Job.Buffers[i]);
        }

        Job.Context    = &Ctx;
        Job.Queue      = &Queue;
        Job.Properties = Mesh.Props;

        return Job;
    }

    enum callbackresult { Ok, Fail /* Aborts the compute job */ }
    alias launchcallback = callbackresult function(int, ref resultvertex[]);

    void
    Launch(double Length, double Fps, launchcallback Callback)
    {
        double Dt = 1 / Fps;
        int    Gens = cast(int)(Length * Fps);
        Launch(Dt, Gens, Callback);
    }

    void
    Launch(double Dt, int Gens, launchcallback Callback)
    {
        size_t GlobalSize = Buffers[0].length;
        size_t LocalSize  = 1;
        double Coeff      = (Dt * Properties.HeatConductivity)
                          / (Properties.HeatCapacity * Properties.Density);

        foreach (Gen; iota(0, Gens)) {
            ulong OtherBufferIdx = (CurrentInputBuffer + 1) % 2;
            /* Set up job */
            FourierKernel.SetArg(0, Buffers[CurrentInputBuffer]);
            FourierKernel.SetArg(1, Coeff);
            FourierKernel.SetArg(2, Buffers[OtherBufferIdx]);
            LaunchKernel(*Queue, FourierKernel, GlobalSize, LocalSize);

            /* Download result buffer */
            CopyToHost(*Queue, Buffers[OtherBufferIdx]);
            auto ResultBuffer = Buffers[OtherBufferIdx]
                .HostBuffer
                .map!(vtx => resultvertex(vtx.X.X, vtx.X.Y, vtx.X.Z, vtx.T, vtx.Id))
                .array;

            if (Callback(Gen, ResultBuffer) == callbackresult.Ok) {
                /* Swap buffers */
                CurrentInputBuffer = OtherBufferIdx;
            } else {
                /* Abort the compute job */
                return;
            }
        }
    }
}
