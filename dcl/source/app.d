import sn.cl;
import clmesh;

import std.algorithm;
import std.format;
import std.range;
import std.stdio;

version (Mandelbrot)
{
    struct MandelComputeJob {
        double center_real, center_imaginary, magnify, color_distortion;
        int    width, height, bailout;
    }

    void
    main()
    {
        "Querying devices".info;
        auto devices = ListClDevices();

        // Choose the first CPU device
        auto device = devices.filter!(x => x.type == CL_DEVICE_TYPE_GPU).front;
        auto devices_to_use = [device];

        "Creating device context".info;
        auto ctx = devices_to_use.CreateContext();

        "Creating a command queue".info;
        auto queue = ctx.CreateCommandQueue(device);

        "Creating program".info;
        auto prog = ctx.CreateProgram(import("mandelbrot.cl"));
    }
}
else version (Mesh)
{
    static enum W = 100, H = 100, dx = 0.005;
    static enum Rho        = 1.23;
    static enum Lambda     = 10.0;
    static enum Cp         = 6e2;


    static int
    Vid(int x, int y)
    {
        assert(x >= 0);
        assert(y >= 0);
        assert(x < W);
        assert(y < H);

        return x + W * y;
    }

    void
    main()
    {
        "Querying devices".info;
        auto Devices = ListClDevices();

        // Choose the first CPU device
        auto Device = Devices.filter!(x => x.type == CL_DEVICE_TYPE_CPU).front;
        auto DevicesToUse = [Device];

        "Creating device context".info;
        auto Ctx = DevicesToUse.CreateContext();

        "Creating a command queue".info;
        auto Queue = Ctx.CreateCommandQueue(Device);

        "Generating mesh".info;
        mesh Mesh;
        Mesh.Verticies.length = W * H;

        for (int Row = 0; Row < H; ++Row) {
            for (int Col = 0; Col < W; ++Col) {
                int     VertexId = Vid(Col, Row);
                double  x        = (cast(double)Col) * dx;
                double  y        = (cast(double)Row) * dx;

                Mesh.Verticies[VertexId] = vertex(x, y, 0, 273.15, VertexId, []);

                if ((Row - 50) * (Row - 50) + (Col - 50) * (Col - 50) < (15 * 15))
                    Mesh.Verticies[VertexId].T += 300;

                /* Add Bonds */
                if (Row > 0) {
                    int Other = Vid(Col, Row - 1);
                    Mesh.AddBond(VertexId, Other);
                }

                if (Col > 0) {
                    int Other = Vid(Col - 1, Row);
                    Mesh.AddBond(VertexId, Other);
                }

                if (Row < H - 1) {
                    int Other = Vid(Col, Row + 1);
                    Mesh.AddBond(VertexId, Other);
                }

                if (Col < W - 1) {
                    int Other = Vid(Col + 1, Row);
                    Mesh.AddBond(VertexId, Other);
                }
            }
        }

        Mesh.Props.Density          = Rho;
        Mesh.Props.HeatConductivity = Lambda;
        Mesh.Props.HeatCapacity     = Cp;

        "Preparing compute job".info;
        auto Job = meshcomputejob.CreateComputeJob(Ctx, Queue, Mesh);

        Job.Launch(20.0, 60.0,
                   (int Gen, ref resultvertex[] Results)
                   {
                       foreach (Result; Results) {
                           "%f\t%f\t%f\t%f"
                               .format(Result.X, Result.Y, Result.Z, Result.T)
                               .writeln;
                       }

                       //"\n".write;

                       return meshcomputejob.callbackresult.Ok;
                   }
        );
    }
}
