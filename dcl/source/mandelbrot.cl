#pragma OPENCL EXTENSION cl_khr_fp64:enable

struct job_descr {
    double center_real, center_imaginary, magnify, color_distortion;
    int    width, height, bailout;
};

/* Implementation of pseudo code :
 * https://en.wikipedia.org/wiki/Mandelbrot_set */
__kernel void
mbrot(struct job_descr descr, __global int *iter_buffer)
{
    int idx = get_global_id(0);

    int x_px = idx % descr.width;
    int y_px = idx / descr.width;

    /* We will not distort the height, only the width */
    double aspect = (double)descr.width / (double)descr.height;
    double mag_factor = 1.0 / descr.magnify;

    double x0 = mix(descr.center_real - 0.5 * mag_factor * aspect,
                    descr.center_real + 0.5 * mag_factor * aspect,
                    (double)(x_px) / (double)(descr.width));
    double y0 = mix(descr.center_imaginary + 0.5 * mag_factor,
                    descr.center_imaginary - 0.5 * mag_factor,
                    (double)(y_px) / (double)(descr.height));

    double x = 0, y = 0;

    int iter = 0;

    /* People are shouting at me! Yes, I know. */
    while (x * x + y * y <= 4 && iter < descr.bailout) {
        double tmp = x * x - y * y + x0;
        y = 2 * x * y + y0;
        x = tmp;
        iter += 1;
    }

    iter_buffer[idx] = iter;
}

/* Because mix doesn't exist for those types ... */
uchar3
lerp(uchar3 a, uchar3 b, double perc)
{
    return (uchar3)(mix((double)a.x, (double)b.x, perc),
                    mix((double)a.y, (double)b.y, perc),
                    mix((double)a.z, (double)b.z, perc));
}

/*
 * Somewhat stolen and adapted from
 * https://git.sr.ht/~blastwave/blastwave/tree/master/item/xmand/mandel_col.c
 */
uchar3
mandel_col(double x)
{
    return (uchar3)(
        clamp(-1.07563e+06 * pow(x, 7) + 3.80666e+06 * pow(x, 6) + -5.30241e+06 * pow(x, 5) + 3.67064e+06 * pow(x, 4) + -1.30583e+06 * pow(x, 3) + 218785.0 * pow(x, 2) + -12212.4 * x + 11.3491, 0.0, 255.0),
        clamp(-172452.0    * pow(x, 7) + 570987.0    * pow(x, 6) + -718725.0    * pow(x, 5) + 417396      * pow(x, 4) + -103012.0    * pow(x, 3) + 4541.99  * pow(x, 2) + 1224.66  * x + 15.1711, 0.0, 255.0),
        clamp(-459718.0    * pow(x, 7) + 1.46659e+06 * pow(x, 6) + -1.76882e+06 * pow(x, 5) + 983891.0    * pow(x, 4) + -236017.0    * pow(x, 3) + 12090.5  * pow(x, 2) + 1923.11  * x + 75.4475, 0.0, 255.0));
}

__kernel void
render(struct job_descr descr, __global int *iter_buffer, __global uchar *output_buffer)
{
    int    idx        = get_global_id(0);
    int    iter_value = iter_buffer[idx];
    double iter_perc  = (double)iter_value / (double)descr.bailout;

    uchar3 color = mandel_col(pow(iter_perc, 1.0 / descr.color_distortion));

    // XXX: THIS ASSUMES LITTLE ENDIANESS
    // XXX: use vstoren
    output_buffer[idx * 4 + 3] = 0xff;
    output_buffer[idx * 4 + 2] = color.z;
    output_buffer[idx * 4 + 1] = color.y;
    output_buffer[idx * 4 + 0] = color.x;
}
