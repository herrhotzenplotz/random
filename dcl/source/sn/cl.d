module sn.cl;

import core.stdc.stdlib : exit;
import std.algorithm;
import std.array;
import std.conv : to;
import std.exception;
import std.format;
import std.range;
import std.stdio;
import std.string;
import std.traits;

/* Reexport the OpenCL symbols */
public import cl;

void
info(string text)
{
    stderr.writeln("INFO : %s".format(text));
}

void
EnsureClSuccess(int code)
{
    static enum error_codes =
        [ "CL_DEVICE_NOT_FOUND", "CL_DEVICE_NOT_AVAILABLE", "CL_COMPILER_NOT_AVAILABLE",
          "CL_MEM_OBJECT_ALLOCATION_FAILURE", "CL_OUT_OF_RESOURCES", "CL_OUT_OF_HOST_MEMORY",
          "CL_PROFILING_INFO_NOT_AVAILABLE", "CL_MEM_COPY_OVERLAP", "CL_IMAGE_FORMAT_MISMATCH",
          "CL_IMAGE_FORMAT_NOT_SUPPORTED", "CL_BUILD_PROGRAM_FAILURE", "CL_MAP_FAILURE",
          "CL_MISALIGNED_SUB_BUFFER_OFFSET", "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST",
          "CL_INVALID_VALUE", "CL_INVALID_DEVICE_TYPE", "CL_INVALID_PLATFORM", "CL_INVALID_DEVICE",
          "CL_INVALID_CONTEXT", "CL_INVALID_QUEUE_PROPERTIES", "CL_INVALID_COMMAND_QUEUE",
          "CL_INVALID_HOST_PTR", "CL_INVALID_MEM_OBJECT", "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR",
          "CL_INVALID_IMAGE_SIZE", "CL_INVALID_SAMPLER", "CL_INVALID_BINARY", "CL_INVALID_BUILD_OPTIONS",
          "CL_INVALID_PROGRAM", "CL_INVALID_PROGRAM_EXECUTABLE", "CL_INVALID_KERNEL_NAME",
          "CL_INVALID_KERNEL_DEFINITION", "CL_INVALID_KERNEL", "CL_INVALID_ARG_INDEX",
          "CL_INVALID_ARG_VALUE", "CL_INVALID_ARG_SIZE", "CL_INVALID_KERNEL_ARGS",
          "CL_INVALID_WORK_DIMENSION", "CL_INVALID_WORK_GROUP_SIZE", "CL_INVALID_WORK_ITEM_SIZE",
          "CL_INVALID_GLOBAL_OFFSET", "CL_INVALID_EVENT_WAIT_LIST", "CL_INVALID_EVENT",
          "CL_INVALID_OPERATION", "CL_INVALID_GL_OBJECT", "CL_INVALID_BUFFER_SIZE",
          "CL_INVALID_MIP_LEVEL", "CL_INVALID_GLOBAL_WORK_SIZE", "CL_INVALID_PROPERTY" ];

    if (code == CL_SUCCESS)
        return;

    static foreach (member; error_codes) {
        if (code == mixin(member)) {
            stderr.writeln("ERR  : %s".format(member));
            exit(1);
        }
    }

    assert(0, "unreachable");
}

string
deviceType(cl_device_type type)
{
    static enum device_types = [ "CL_DEVICE_TYPE_CPU", "CL_DEVICE_TYPE_GPU",
                                 "CL_DEVICE_TYPE_ACCELERATOR", "CL_DEVICE_TYPE_DEFAULT",
                                 "CL_DEVICE_TYPE_ALL" ];
    static foreach (device_type; device_types) {
        if (type == mixin(device_type))
            return device_type;
    }

    assert(0, "unreachable");
}

struct ClDevice
{
    string         name;
    cl_device_type type;
    cl_device_id   id;
    cl_platform_id platform;
}

struct ClContext
{
    ClDevice[] *devices;
    cl_context  context;
}

struct ClBuffer(T)
{
    T[] HostBuffer;
    cl_mem mem;

    alias HostBuffer this;
}

// TODO: Handle automatic host-to-device copy
ClBuffer!T
CreateBuffer(T)(ref ClContext ctx, ulong size, cl_mem_flags flags)
{
    ClBuffer!T result;
    int err;

    assert(size > 0, "Cannot create ClBuffer of size 0");

    scope(exit) err.EnsureClSuccess;

    result.length = size;
    result.mem = ctx.context.clCreateBuffer(flags|CL_MEM_COPY_HOST_PTR,
                                            size * T.sizeof,
                                            result.HostBuffer.ptr, &err);

    return result;
}

alias ClProgram = cl_program;
alias ClKernel  = cl_kernel;

ClProgram
CreateProgram(ref ClContext ctx, string source)
{
    cl_program     p;
    cl_int         err;
    cl_device_id[] device_list;

    auto src     = source.ptr;
    auto src_len = source.length;

    p = clCreateProgramWithSource(ctx.context, 1, &src, &src_len, &err);
    err.EnsureClSuccess;

    device_list = map!(x => x.id)(*ctx.devices).array;

    err = clBuildProgram(p, cast(cl_uint)(ctx.devices.length), device_list.ptr, null, null, null);

    if (err == CL_BUILD_PROGRAM_FAILURE) {

        stderr.writeln("ERR : There were build failures:");

        foreach (device; *ctx.devices) {

            "ERR : On device '%s':".format(device.name).writeln(stderr);

            size_t log_size;
            char[] log;
            err = clGetProgramBuildInfo(p, device.id, CL_PROGRAM_BUILD_LOG, 0, null, &log_size);
            err.EnsureClSuccess;

            log.length = log_size;
            err = clGetProgramBuildInfo(p, device.id, CL_PROGRAM_BUILD_LOG, log_size, log.ptr, null);

            stderr.writeln(log);
        }

        exit(1);
    } else {
        err.EnsureClSuccess;
    }

    return p;
}

ClKernel
CreateKernel(ClProgram program, string kernel)
{
    cl_int err;
    scope(exit) err.EnsureClSuccess;
    return program.clCreateKernel(kernel.toStringz, &err);
}

/+
 + These functions copy the passed values to the device.
 +/
void
SetArg(T)(ClKernel k, cl_uint arg_idx, T arg)
    if (isScalarType!T)
{
    cl_int err;
    k.clSetKernelArg(arg_idx, arg.sizeof, &arg);
    err.EnsureClSuccess;
}

void
SetArg(T)(ClKernel k, cl_uint arg_idx, ref T arg)
{
    cl_int err;

    static if (isInstanceOf!(ClBuffer, T)) {
        k.clSetKernelArg(arg_idx, arg.mem.sizeof, &arg.mem);
    } else {
        k.clSetKernelArg(arg_idx, arg.sizeof, &arg);
    }

    err.EnsureClSuccess;
}

ClContext
CreateContext(return ref ClDevice[] devices)
{
    cl_int err;
    cl_context ctx;
    cl_device_id[] device_ids;

    device_ids = devices.map!(x => x.id).array;

    ctx = clCreateContext(null, cast(uint)(device_ids.length), device_ids.ptr,
                          null, null, &err);
    err.EnsureClSuccess;

    return ClContext(&devices, ctx);
}

ClDevice[]
ListClDevices(bool verbose = true)
{
    ClDevice[]       result;
    cl_uint          num_platforms;
    cl_platform_id[] platforms;

    // Read platforms
    EnsureClSuccess(clGetPlatformIDs(0, null, &num_platforms));
    enforce(num_platforms > 0, "No platforms available");

    platforms.length = num_platforms;

    EnsureClSuccess(clGetPlatformIDs(num_platforms, platforms.ptr, &num_platforms));

    foreach (platform; platforms) {
        char[64]       name;
        size_t         name_len = 0;
        cl_device_id[] devices;
        cl_uint        num_devices;

        // print the platform name
        EnsureClSuccess(clGetPlatformInfo(platform, CL_PLATFORM_NAME, name.sizeof, name.ptr, &name_len));

        EnsureClSuccess(clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, 0, null, &num_devices));
        if (num_devices == 0) {
            "    No devices in this platform".info;
            continue;
        }

        devices.length = num_devices;

        EnsureClSuccess(clGetDeviceIDs(platform, CL_DEVICE_TYPE_ALL, num_devices, devices.ptr, &num_devices));
        foreach (device; devices) {
            char[1024]     device_name;
            size_t         device_name_len;
            cl_device_type device_type;
            EnsureClSuccess(clGetDeviceInfo(device, CL_DEVICE_NAME, device_name.sizeof, device_name.ptr, &device_name_len));
            EnsureClSuccess(clGetDeviceInfo(device, CL_DEVICE_TYPE, device_type.sizeof, &device_type, null));
            result ~= ClDevice(device_name[0..device_name_len].dup.fromStringz,
                               device_type,
                               device,
                               platform);
        }

        devices.length = 0;
    }

    return result;
}

struct ClCommandQueue
{
    cl_command_queue  queue;
    ClContext        *ctx;
    ClDevice         *device;
}

ClCommandQueue
CreateCommandQueue(ref return ClContext ctx,
                   ref return ClDevice  dev)
{
    cl_command_queue q;
    cl_int           err;

    assert(canFind(*ctx.devices, dev),
           "Device not in context for command queue to be created");

    q = clCreateCommandQueue(ctx.context, dev.id, 0, &err);
    err.EnsureClSuccess;

    return ClCommandQueue(q, &ctx, &dev);
}

void
CopyToDevice(T)(ref ClCommandQueue q,
                ref ClBuffer!T     buf)
{
    assert(buf.length > 0, "ClBuffer is uninitialized. Use CreateBuffer to initialize the buffer");

    q.queue
        .clEnqueueWriteBuffer(buf.mem, true, 0,
                              T.sizeof * buf.length, buf.ptr,
                              0, null, null)
        .EnsureClSuccess;
}

void
CopyToHost(T)(ref ClCommandQueue q,
              ref ClBuffer!T     buf)
{
    assert(buf.length > 0, "ClBuffer is uninitialized. Use CreateBuffer to initialize the buffer");

    q.queue
        .clEnqueueReadBuffer(buf.mem, true, 0,
                             T.sizeof * buf.length, buf.ptr,
                             0, null, null)
        .EnsureClSuccess;
    q.queue.clFinish.EnsureClSuccess;
}

void
LaunchKernel(ref ClCommandQueue q,
             ref ClKernel       krnl,
             ref size_t         global_size,
             ref size_t         local_size)
{
    q.queue
        .clEnqueueNDRangeKernel(krnl, 1, null, &global_size, &local_size, 0, null, null)
        .EnsureClSuccess;
}
