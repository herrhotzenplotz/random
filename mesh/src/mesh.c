#include <mesh/mesh.h>

#include <assert.h>
#include <err.h>
#include <fcntl.h>
#include <math.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

static cl_device_id
create_device(void)
{
	cl_platform_id	platform;
	cl_device_id	dev;
	int				error;

	error = clGetPlatformIDs(1, &platform, NULL);
	if(error < 0)
		err(error, "Couldn't identify a platform");

	error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &dev, NULL);
	if(error == CL_DEVICE_NOT_FOUND) {
		error = clGetDeviceIDs(platform, CL_DEVICE_TYPE_CPU, 1, &dev, NULL);
	}

	if(error < 0)
		err(error, "Couldn't access any devices");

	return dev;
}

void
mesh_init(mesh *it, size_t verts)
{
	assert(it);

	it->verticies_size = verts;
	it->verticies      = calloc(verts, sizeof(vertex));

	/* CL Initialization */
	it->cl.device_id = create_device();
	it->cl.device_context = clCreateContext(NULL, 1, &it->cl.device_id, NULL, NULL, NULL);
	it->cl.device_queue   = clCreateCommandQueueWithProperties(it->cl.device_context,
															   it->cl.device_id, NULL, NULL);

	int fd = open("src/fourier.cl", O_RDONLY);
	if (fd < 0)
		err(1, "open");

	struct stat st = {0};
	if (fstat(fd, &st) < 0)
		err(1, "fstat");

	const char *src = mmap(NULL, st.st_size, PROT_READ, MAP_PRIVATE, fd, 0);
	if (!src)
		err(1, "mmap");

	it->cl.program = clCreateProgramWithSource(it->cl.device_context, 1, &src, (const size_t *)&st.st_size, NULL);
	clBuildProgram(it->cl.program, 0, NULL, NULL, NULL, NULL);
	{
		size_t log_size;
		clGetProgramBuildInfo(it->cl.program, it->cl.device_id, CL_PROGRAM_BUILD_LOG, 0, NULL, &log_size);
		char *program_log = (char*) malloc(log_size);
		clGetProgramBuildInfo(it->cl.program, it->cl.device_id, CL_PROGRAM_BUILD_LOG,
							  log_size + 1, program_log, NULL);
		printf("%.*s\n", (int)log_size, program_log);
		free(program_log);
	}

	munmap((void *)(src), st.st_size);
	close(fd);

	it->cl.kernel = clCreateKernel(it->cl.program, "fourier", NULL);
}

void
mesh_add_bond(mesh *it, int from, int to)
{
	for (size_t i = 0; i < it->verticies_size; ++i)
		if (it->verticies[i].id == from) {
			vertex_add_bond(it->verticies + i, to);
			return;
		}

	errx(1, "%s: %d: error in %s: target vertex %d is not a member of the mesh", __FILE__, __LINE__, __func__, from);
}

void
mesh_dump(mesh *it, FILE *output)
{
	assert(it);
	assert(output);

	for (size_t i = 0; i < it->verticies_size; ++i)
		vertex_dump(it->verticies + i, output);
}

void
mesh_step(mesh *it, real dTime)
{
	real *new_ts = calloc(it->verticies_size, sizeof(double));
	if (!new_ts)
		err(1, "calloc");

	real step_coefficient = (dTime * it->props.heat_conductivity) / (it->props.heat_capacity * it->props.density);
	assert(step_coefficient < 0.25);

	cl_mem input_buffer = clCreateBuffer(it->cl.device_context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR,
										 it->verticies_size * sizeof(vertex),
										 it->verticies, NULL);
	cl_mem output_buffer = clCreateBuffer(it->cl.device_context, CL_MEM_WRITE_ONLY | CL_MEM_COPY_HOST_PTR,
										  it->verticies_size * sizeof(real), new_ts, NULL);

	clSetKernelArg(it->cl.kernel, 0, sizeof(input_buffer), &input_buffer);
	clSetKernelArg(it->cl.kernel, 1, sizeof(real), &step_coefficient);
	clSetKernelArg(it->cl.kernel, 2, sizeof(output_buffer), &output_buffer);

	const size_t global_size = it->verticies_size;
	const size_t local_size  = 5;

	if (clEnqueueNDRangeKernel(it->cl.device_queue, it->cl.kernel, 1, NULL, &global_size, &local_size, 0, NULL, NULL) < 0)
		err(1, "cannot enqueue kernel");

	if (clEnqueueReadBuffer(it->cl.device_queue, output_buffer,
							CL_TRUE, 0, sizeof(real) * it->verticies_size, new_ts, 0, NULL, NULL) < 0)
		err(1, "failed to read back buffer");

	for (size_t i = 0; i < it->verticies_size; ++i)
		it->verticies[i].T = new_ts[i];

	clReleaseMemObject(input_buffer);
	clReleaseMemObject(output_buffer);

	free(new_ts);
}
