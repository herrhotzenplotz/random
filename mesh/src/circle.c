#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <math.h>

#include <mesh/mesh.h>

#define N 300

static int
modulo(int a, int b)
{
	return ((a % b) + b) % b;
}

int
main(void)
{
	mesh	mesh = {0};
	real	R;

	fputs("poisson-mesh 0", stderr);

	mesh_init(&mesh, N);

	// Let's make this a circle of verticies.
	R = .01;

	for (int i = 0; i < N; ++i) {
		real x, y, z, angle;
		int  left_neighbor, right_neighbor;

		angle = (real)i * 2.0 * M_PI / (real)N;

		x = cos(angle) * R;
		y = sin(angle) * R;
		z = cos(angle);

		vertex_init(mesh.verticies + i, i, V3(x, y, z));
		mesh.verticies[i].T = 273.15;
		mesh_add_bond(&mesh, i, modulo(i - 1, N));
		mesh_add_bond(&mesh, i, modulo(i + 1, N));
	}

	mesh.verticies[3].T += 300;

	mesh.props.density = 1.3;
	mesh.props.heat_conductivity = 9.3e-1;
	mesh.props.heat_capacity = 7.6e2;

	FILE *output = stdout;//fopen("circle.dat", "w");
	for (size_t i = 0; i < 1000; ++i) {
		mesh_dump(&mesh, output);
		mesh_step(&mesh, 0.001);
		fputc('\n', output);
	}

	fclose(output);

	return (EXIT_SUCCESS);
}
