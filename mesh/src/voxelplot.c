#include <err.h>
#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <raylib.h>

float	dx			= 1;
int		generations = 0;
int     N           = 300;
float   fps         = 60.0f;

static void
usage(void)
{
	fprintf(stderr,
			"usage: voxelplot [-h] [-n no-of-elems] [-d dx] [-f fps] [input file]\n");
}

static float
lerp(float x, float x0, float x1, float y0, float y1)
{
	return y0 + x * (y1 - y0) / (x1 - x0);
}

static Color
heatscale(float x, float x0, float x1)
{
	float f = lerp(x, x0, x1, 0.0f, 1.0f);
	return ColorFromHSV(fmod((1.0 - f) * 240 + 180, 360), 1.0, 0.5);
}

int
main(int argc, char *argv[])
{
	int		  ch, gen;
	Vector4 **points = NULL;
	FILE	 *f		 = NULL;
	Camera	  cam	 = {
		.position	= { 0.05f, 0.05f, 0.2f },
		.target		= { 0.05f, 0.05f, 0.0f },
		.up			= { 0.0f, 0.05f, 0.0f },
		.fovy		= 45.0f,
		.projection = CAMERA_PERSPECTIVE
	};

	while ((ch = getopt(argc, argv, "hd:n:f:")) != -1) {
		switch (ch) {
		case 'n':
			N = strtol(optarg, NULL, 10);
			break;
		case 'd':
			dx = strtof(optarg, NULL);
			break;
		case 'f':
			fps = strtof(optarg, NULL);
			break;
		case 'h':
		case '?':
		default:
			usage();
			exit(1);
		}
	}

	argc -= optind;
	argv += optind;

	f = argc ? fopen(argv[0], "r") : stdin;
	if (!f)
		err(1, "fopen");

	SetTraceLogLevel(LOG_ERROR);
	InitWindow(1024, 786, "voxelplot");
	SetTargetFPS((int)fps);

	SetCameraMode(cam, CAMERA_FREE);

	gen = 0;
	while (!WindowShouldClose()) {

		if (f) {
			if (!feof(f)) {
				points = realloc(points, sizeof(Vector4 *) * (generations + 1));
				points[generations++] = malloc(N * sizeof(Vector4));
				for (size_t i = 0; i < N; ++i) {
					float foo;
					if (fscanf(f, "%f\t%f\t%f\t%f\n",
							   &points[generations - 1][i].x,
							   &points[generations - 1][i].y,
							   &points[generations - 1][i].z,
							   &points[generations - 1][i].w) != 4)
						errx(1, "illegal data-format");
				}
				fscanf(f, "\n");
			} else {
				fclose(f);
				f = NULL;
			}
		}

		UpdateCamera(&cam);

		BeginDrawing();
		ClearBackground(BLACK);
		BeginMode3D(cam);

		for (size_t i = 0; i < N; ++i) {
			DrawCube((Vector3) { .x = points[gen][i].x, .y = points[gen][i].y, .z = points[gen][i].z },
					 dx, dx, dx, heatscale(points[gen][i].w, 200, 500));
		}

		EndMode3D();

		DrawText(TextFormat("voxelplot - gen %d\n%s", gen, f ? "still reading input..." : "finished reading input"), 5, 5, 20, LIGHTGRAY);

		EndDrawing();

		float render_time = GetFrameTime();
		float delta_time  = 1.0f / fps;

		if ((render_time < delta_time) && !f)
			usleep((int)(fps - render_time) * 1e6);

		gen = (gen + 1) % generations;
	}

	CloseWindow();

	if (points) {
		for (size_t g = 0; g < generations; ++g) {
			free(points[g]);
		}

		free(points);
	}

	return 0;
}
