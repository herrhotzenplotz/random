typedef struct v3 v3;
struct v3 {
	double x, y, z;
};

typedef struct vertex vertex;
struct vertex {
	v3	x;
	double	T;
	int		id;
	int		bonds[100];
	size_t	bonds_size;
};

double3
vtod(v3 it)
{
	return (double3)(it.x, it.y, it.z);
}

__kernel void
fourier(__global vertex *in, double coefficient, __global double *out)
{
		int vid = get_global_id(0);

		double slope = 0.0;

		for (size_t bidx = 0; bidx < in[vid].bonds_size; ++bidx) {
			slope += (in[vid].T - in[in[vid].bonds[bidx]].T) / distance(vtod(in[vid].x), vtod(in[in[vid].bonds[bidx]].x));
		}

		out[vid] = in[vid].T - slope * coefficient;
}
