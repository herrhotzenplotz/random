#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <math.h>
#include <unistd.h>

#include <mesh/mesh.h>

#define W 100
#define H 100
#define N (W * H)

static int
modulo(int a, int b)
{
	return ((a % b) + b) % b;
}

static int
vid(int x, int y)
{
	assert(x >= 0);
	assert(y >= 0);
	assert(x < W);
	assert(y < H);

	return x + W * y;
}

static void
dump(mesh *mesh, FILE *output)
{
	for (int y = 0; y < H; ++y) {
		for (int x = 0; x < W; ++x) {
			int vertex_id = vid(x, y);

			fprintf(output, "\t%10.4lf", mesh->verticies[vertex_id].T);
		}

		fputc('\n', output);
	}
}

int
main(int argc, char *argv[])
{
	mesh	mesh		= {0};
	real	dx			= 0.001;
	size_t  generations = 500;
	real    dt          = 0.1;
	real    rho         = 1.3;
	real    lambda      = 9.3e-1;
	real    cp          = 7.6e2;

	int ch;
	while ((ch = getopt(argc, argv, "i:t:r:l:c:d:")) != -1) {
		switch (ch) {
		case 'i':
			generations = strtoul(optarg, NULL, 10);
			break;
		case 't':
			dt = strtod(optarg, NULL);
			break;
		case 'r':
			rho = strtod(optarg, NULL);
			break;
		case 'c':
			cp = strtod(optarg, NULL);
			break;
		case 'd':
			dx = strtod(optarg, NULL);
			break;
		case 'l':
			lambda = strtod(optarg, NULL);
			break;
		case '?':
		default:
			errx(1, "unknown option");
		}
	}

	argc -= optind;
	argv += optind;

	fputs("plane - mesh 0\n", stderr);

	mesh_init(&mesh, N);

	fputs("generating mesh...\n", stderr);
	for (int row = 0; row < H; ++row) {
		for (int col = 0; col < W; ++col) {
			int		vertex_id = vid(col, row);
			real	x		  = ((real)col) * dx;
			real	y		  = ((real)row) * dx;

			vertex_init(mesh.verticies + vertex_id, vertex_id, V3(x, y, 0.0));

			mesh.verticies[vertex_id].T = 273.15;

			if ((row - 50) * (row - 50) + (col - 50) * (col - 50) < (15 * 15))
				mesh.verticies[vertex_id].T += 300;

			/* Add Bonds */
			if (row > 0) {
				int other = vid(col, row - 1);
				mesh_add_bond(&mesh, vertex_id, other);
			}

			if (col > 0) {
				int other = vid(col - 1, row);
				mesh_add_bond(&mesh, vertex_id, other);
			}

			if (row < H - 1) {
				int other = vid(col, row + 1);
				mesh_add_bond(&mesh, vertex_id, other);
			}

			if (col < W - 1) {
				int other = vid(col + 1, row);
				mesh_add_bond(&mesh, vertex_id, other);
			}
		}
	}

	mesh.props.density = rho;
	mesh.props.heat_conductivity = lambda;
	mesh.props.heat_capacity = cp;

	fputs("performing simulation...\n", stderr);

	FILE *output = argc ? fopen("foo.dat", "w") : stdout;
	for (size_t i = 0; i < generations; ++i) {
		fprintf(stderr, "\r%zu/%zu", i + 1, generations);
		fflush(stdout);

		mesh_dump(&mesh, output);
		mesh_step(&mesh, dt);
		fputc('\n', output);
	}

	fclose(output);

	return (EXIT_SUCCESS);
}
