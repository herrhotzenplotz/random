#include <mesh/vertex.h>

#include <assert.h>
#include <err.h>
#include <stdlib.h>

void
vertex_init(vertex *it, int id, v3 x)
{
	if (!it)
		err(1, "initializing invalid vertex");

	it->x		   = x;
	it->id		   = id;
	it->bonds_size = 0;
}

void
vertex_destroy(vertex *it)
{
	if (!it)
		return;
}

static int
vertex_has_bond_to(vertex *it, int to)
{
	for (size_t i = 0; i < it->bonds_size; ++i)
		if (it->bonds[i] == to)
			return 1;
	return 0;
}

void
vertex_add_bond(vertex *it, int the_bond)
{
	if (vertex_has_bond_to(it, the_bond)) {
		warnx("attempting to add already existing bond from vertex %d to %d",
			  it->id, the_bond);
		return;
	}


	it->bonds[it->bonds_size++] = the_bond;
}

void
vertex_dump(vertex *it, FILE *output)
{
	assert(it);
	assert(output);

	fprintf(output, "%lf\t%lf\t%lf\t%lf\n", it->x.x, it->x.y, it->x.z, it->T);
}
