#include <mesh/v3.h>

inline v3
V3(real x, real y, real z)
{
	return (v3) {
		.x = x,
		.y = y,
		.z = z,
	};
}
