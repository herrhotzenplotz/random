#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <err.h>
#include <math.h>
#include <unistd.h>

#include <mesh/mesh.h>

static int		W			= 15;
static int		H			= 15;
static int		D			= 15;
static real		dx			= 0.001;
static size_t	generations = 500;
static real		dt          = 0.1;
static real		rho         = 1.3;
static real		lambda      = 9.3e-1;
static real		cp          = 7.6e2;

#define N (W * H * D)

static int
vid(int x, int y, int z)
{
	assert(x >= 0);
	assert(y >= 0);
	assert(x < W);
	assert(y < H);

	return x + W * y + z * W * H;
}

int
main(int argc, char *argv[])
{
	mesh	mesh = {0};

	int ch;
	while ((ch = getopt(argc, argv, "i:t:r:l:c:d:x:y:z:")) != -1) {
		switch (ch) {
		case 'i':
			generations = strtoul(optarg, NULL, 10);
			break;
		case 't':
			dt = strtod(optarg, NULL);
			break;
		case 'r':
			rho = strtod(optarg, NULL);
			break;
		case 'c':
			cp = strtod(optarg, NULL);
			break;
		case 'd':
			dx = strtod(optarg, NULL);
			break;
		case 'l':
			lambda = strtod(optarg, NULL);
			break;
		case 'x':
			W = strtol(optarg, NULL, 10);
			break;
		case 'y':
			H = strtol(optarg, NULL, 10);
			break;
		case 'z':
			D = strtol(optarg, NULL, 10);
			break;
		case '?':
		default:
			errx(1, "unknown option");
		}
	}

	argc -= optind;
	argv += optind;

	fputs("cube0\n", stderr);

	mesh_init(&mesh, N);

	fputs("generating mesh...\n", stderr);
	for (int layer = 0; layer < D; ++layer) {
		for (int row = 0; row < H; ++row) {
			for (int col = 0; col < W; ++col) {
				int		vertex_id = vid(col, row, layer);
				real	x		  = ((real)col) * dx;
				real	y		  = ((real)row) * dx;
				real	z		  = ((real)layer) * dx;

				vertex_init(mesh.verticies + vertex_id, vertex_id, V3(x, y, z));

				mesh.verticies[vertex_id].T = 273.15;

				if (layer <= 5 && col <= 5 && row <= 5)
					mesh.verticies[vertex_id].T += 400;

				/* Add Bonds */
				if (row > 0) {
					int other = vid(col, row - 1, layer);
					mesh_add_bond(&mesh, vertex_id, other);
				}

				if (col > 0) {
					int other = vid(col - 1, row, layer);
					mesh_add_bond(&mesh, vertex_id, other);
				}

				if (row < H - 1) {
					int other = vid(col, row + 1, layer);
					mesh_add_bond(&mesh, vertex_id, other);
				}

				if (col < W - 1) {
					int other = vid(col + 1, row, layer);
					mesh_add_bond(&mesh, vertex_id, other);
				}

				if (layer > 0) {
					int other = vid(col, row, layer - 1);
					mesh_add_bond(&mesh, vertex_id, other);
				}

				if (layer < D - 1) {
					int other = vid(col, row, layer + 1);
					mesh_add_bond(&mesh, vertex_id, other);
				}
			}
		}
	}

	mesh.props.density = rho;
	mesh.props.heat_conductivity = lambda;
	mesh.props.heat_capacity = cp;

	fputs("performing simulation...\n", stderr);

	FILE *output = argc ? fopen("foo.dat", "w") : stdout;
	for (size_t i = 0; i < generations; ++i) {
		fprintf(stderr, "\r%zu/%zu", i + 1, generations);
		fflush(stdout);

		mesh_dump(&mesh, output);
		mesh_step(&mesh, dt);
		fputc('\n', output);
	}

	fclose(output);

	return (EXIT_SUCCESS);
}
