#ifndef MESH_H
#define MESH_H

#include <mesh/vertex.h>

#include <stdio.h>
#include <stdlib.h>

#define CL_TARGET_OPENCL_VERSION 220
#include <CL/cl.h>

typedef struct mesh mesh;
typedef struct material_props material_props;

struct material_props {
	real density, heat_capacity, heat_conductivity;
};

struct mesh {
	size_t				 verticies_size;
	vertex				*verticies;
	material_props		 props;
	struct cl {
		cl_device_id		device_id;
		cl_context			device_context;
		cl_command_queue	device_queue;
		cl_program			program;
		cl_kernel           kernel;
	} cl;
};

void mesh_init(mesh *, size_t verts);
void mesh_dump(mesh *, FILE *);
void mesh_add_bond(mesh *, int, int);
void mesh_step(mesh *, real);

#endif /* MESH_H */
