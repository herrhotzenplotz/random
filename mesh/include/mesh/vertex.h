#ifndef VERTEX_H
#define VERTEX_H

#include <mesh/v3.h>
#include <stdio.h>
#include <sys/queue.h>

typedef struct vertex vertex;

struct vertex {
	v3	x;
	real	T;
	int	id;
	int	bonds[100];
	size_t	bonds_size;
};

void vertex_init(vertex *, int, v3);
void vertex_destroy(vertex *);
void vertex_add_bond(vertex *, int);
void vertex_dump(vertex *, FILE *);

#endif /* VERTEX_H */
