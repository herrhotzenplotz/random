#ifndef V3_H
#define V3_H

#include <mesh/fp.h>

typedef struct v3 v3;

struct v3 {
	real x, y, z;
};

v3 V3(real x, real y, real z);

#endif /* V3_H */
